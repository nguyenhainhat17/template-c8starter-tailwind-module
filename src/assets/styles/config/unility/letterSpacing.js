const letterSpacing = {
  tighter: '-0.05em',
  tight: '-0.025em',
  normal: '0',
  wide: '0.025em',
  'wide-than': '0.04em',
  wider: '0.05em',
  'wider-than': '0.07em', 
  widest: '0.1em',
  'sp1': '-.33px',
  'sp2': '1px',
  'sp3': '0.31px',
  'sp4': '0.14px',
  'sp5': '0.5px',
  'sp6': '-.11px',
  'sp7': '-.17px'

}
module.exports = {
  letterSpacing
}
