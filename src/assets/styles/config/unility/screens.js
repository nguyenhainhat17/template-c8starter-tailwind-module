const Screens = {
  'sm': '481px',
  'md': '768px',
  'lg': '992px',
  'tablet': '1025px',
  'xl': '1200px',
  '2xl': '1440px',
  '2k': '2000px',
  'down_xl': {'max': '1199px'},
  'down_lg': {'max': '991px'},
  'down_md': {'max': '767px'},
  'down_sm': {'max': '480px'},
  'md_to_lg': {'min': '768px', 'max': '991px'},
  'md_to_2xl': {'min': '768px', 'max': '1439px'},
  'md_to_xl': {'min': '768px', 'max': '1199px'},
  'lg_to_xl': {'min': '992px', 'max': '1199px'},
  'lg_to_2xl': {'min': '992px', 'max': '1439px'},
  'xl_to_2xl': {'min': '1200px', 'max': '1439px'},
  '375_to_mxl': {'min': '375px', 'max': '1280px'},
}
module.exports = {
  Screens
}
