const minHeight = {
  0: '0',
  full: '100%',
  screen: '100vh',
  'banner-top': '680px',
  'banner-lg': '685px',
  'banner-bottom': '690px',
  'banner-mobile': '696px',
}
module.exports = {
  minHeight
}
