const Colors = {
  primary: '#b94a02',
  green: {
    100: '#576541',
    200: '#495735'
  },
  yellow: '#e0aa2a',
  brown: {
    100: '#b94a02',
    200: '#A33100',
    300: '#dfc8a0',
    400: '#8a2432'
  },
  gray: {
    50: '#ffffff80',
    100: '#fcf7ef',
    150: '#a3aaae',
    200: '#F7F1E7',
    300: '#f7f7f7;'
  },
  blue: {
    100: '#1d4f91',
    200: '#1e5091',
    300: '#0a2240',
    400: '#468bc9'
  },
  black: '#414042',
  transparent: 'transparent'
}
module.exports = {
  Colors
}
