export default class SliderDemo {
  constructor () {
    this.$this = $('.mod-textImg-slider')
  }
  init () {
    if (this.$this.length) {
      this.addSlick()
    }
  }
  addSlick () {
    this.$this.find('.slider').slick({
      rows: 0,
      zIndex: 1000,
      // slidesToShow: 1,
      // slidesToScroll: 1,
      // autoplay: true,
      // autoplaySpeed: 3000,
      appendArrows: $('.slider-arrow'),
      prevArrow: '<button type="button" class="slick-prev arrows"><span class="icomoon h1 icon-chevron-left"></span></button>',
      nextArrow: '<button type="button" class="slick-next arrows"><span class="icomoon h1 icon-chevron-right"></span></button>'
    
    })
  }
}
new SliderDemo().init()
